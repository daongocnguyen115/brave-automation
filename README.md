tutorial cai dat vps
```angular2html
https://www.youtube.com/watch?v=4s3aegOH3nc
```
swap memory
```angular2html
https://vinasupport.com/huong-dan-them-bo-nho-swap-tren-ubuntu-centos-linux/
```
update node version
```angular2html
https://askubuntu.com/questions/426750/how-can-i-update-my-nodejs-to-the-latest-version
```

install git on linux
```angular2html
https://linuxtechlab.com/install-git-linux-ubuntu-centos/
```
cai dat chrome tren linux
```
https://www.wikihow.com/Install-Google-Chrome-Using-Terminal-on-Linux
```
check location chrome on linux
```
whereis google-chrome
```
tao folder tren linux
```
https://www.cyberciti.biz/faq/how-to-make-a-folder-in-linux-or-unix/
```


check nohup
```angular2html
xem file log: tail nohup.out
xem list job nohup: ps -aux |grep node
hoac: pgrep –a
kill job: kill -9 PID

```

# setup
```angular2html
1:
sudo apt-get update
sudo apt-get upgrade
sudo apt-get install git

2 nodejs:
curl -fsSL https://deb.nodesource.com/setup_current.x | sudo -E bash -
sudo apt-get install -y nodejs

3 tao folder:
mkdir -m 777 chainflix
cd chainflix

4 source git:
git clone https://gitlab.com/daongocnguyen115/brave-automation.git .
git checkout linux-version

5: install brave
sudo apt install apt-transport-https curl

sudo curl -fsSLo /usr/share/keyrings/brave-browser-archive-keyring.gpg https://brave-browser-apt-release.s3.brave.com/brave-browser-archive-keyring.gpg

echo "deb [signed-by=/usr/share/keyrings/brave-browser-archive-keyring.gpg arch=amd64] https://brave-browser-apt-release.s3.brave.com/ stable main"|sudo tee /etc/apt/sources.list.d/brave-browser-release.list

sudo apt update

sudo apt install brave-browser

6. npm install --force
```

### get source location
exp:
- chainflix source: /home/ysidakhoa192/chainflix
- brave browser: /usr/bin/brave-browser
```angular2html
pwd
whereis brave-browser
```
copy 2 dia chi ben tren lai

### edit env
run command
```angular2html
nano .env
```
edit content to
```angular2html
BROWSER_APPLICATION_PATH=/usr/bin/brave-browser
BROWSER_PROFILE_PATH=/home/ysidakhoa192/chainflix/browsers
```

### edit account
file accountLogin.txt
sua theo cau truc email|password
```angular2html
nano accountLogin.txt
```

Tao file nohup
```angular2html
touch nohup.out
```
# run
```angular2html
sudo nohup node main.js &
```
