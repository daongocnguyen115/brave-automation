const puppeteer = require("puppeteer-extra");
const {randomScrollArr} = require("../actions/utils");
const {sleep} = require("../actions/utils");
const {autoScroll} = require("../actions/scroll");
const {initTask} = require("../actions/task");

// Add stealth plugin and use defaults (all tricks to hide puppeteer usage)
const StealthPlugin = require("puppeteer-extra-plugin-stealth");
const {campaignDetect} = require("../actions/campaignDetect");
const {reloadPage} = require("../actions/reloadPage");
const {onNewBrave} = require("../actions/startingNewBrave");
puppeteer.use(StealthPlugin());

const {autoNavigate, getRandomUrl} = require("../actions/navigate");

let profileNumber = 0
let configs = {}

async function braveTopic(config) {
  profileNumber = 0
  configs = config
  const user = config.name
  await main(configs.brave.soLuongBrowserProfile[0], user);

  // console.log(`task: browser user ${user} all done with ${profileNumber + 1} profile`);
}

async function main(profileName = 'Default', user) {
  // const webUrl = initTask()
  //
  // console.log('============================');
  // console.log('open url:', webUrl);
  // console.log('============================');

  const browser = await puppeteer.launch({
    headless:false,
    ignoreHTTPSErrors: true,
    args: [`--window-size=1200,700`, '--test-type', `--profile-directory=${profileName}`],
    ignoreDefaultArgs: ['--enable-automation'],
    defaultViewport: {
      width:1200,
      height:700
    },
    executablePath: `${process.env.BROWSER_APPLICATION_PATH}`,
    userDataDir: `${process.env.BROWSER_PROFILE_PATH}/${user}`,
  });
  let pages = await browser.pages()
  var page = pages[0];

  await sleep(1000);

  for (let i = 1; i < pages.length; i++) {
    pages[i].close();
  }
  // page.setUserAgent('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36');

  try {
    await page.goto('brave://newtab/',{timeout: 5000});
    // await page.goto(webUrl, {timeout: 5000});
  } catch (e) {
    console.log('errorrrrr', e);
  }

  await sleep(1000);

  // check is new brave
  await onNewBrave(page)
  // check is new brave

  await sleep(1000);
  await reloadPage(page, 1)


  await sleep(1000);
  // detect campaign
  const campaign = await campaignDetect(page)

  switch (campaign) {
    case "backgroundAds":
      await reloadPage(page, 20)
      break;
    default:
      break;
  }

  await browser.close();
  if (profileNumber < configs.brave.soLuongBrowserProfile.length - 1) {
    profileNumber += 1
    await sleep(2000)
    await main(configs.brave.soLuongBrowserProfile[profileNumber], configs.name);
  } else {
    return false
  }
  // else {
  //   console.log('============================');
  //   console.log('WAIT FOR NEW CAMPAIGN');
  //   console.log('============================');
  //   await sleep(10000);
  //   console.log('============================');
  //   console.log('STARTING NEW CAMPAIGN');
  //   console.log('============================');
  //   profileNumber = 0
  //   await main(`Default`, configs.name);
  // }
}

module.exports = {
  braveTopic
}
