const {login} = require("./isLogged");
const {isLogged} = require("./isLogged");
const {sleep} = require("../actions/utils");


const detectLogin = async (page, configs) => {

  const isLogin = await isLogged(page, configs.chainFlix.account.email)

  if (!isLogin) {
    await login(page, configs)
  } else {
    console.log('has logged');
  }
  return true
}


module.exports = {
  detectLogin
}
