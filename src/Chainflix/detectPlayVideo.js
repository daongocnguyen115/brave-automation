const playVideo = async (page) => {

  const duration = await page.evaluate(async () => {
    const sleep = (ms) => {
      return new Promise((resolve) => setTimeout(resolve, ms));
    }

    const getDuration = (video) => {
      return new Promise(resolve => {
        var interval = setInterval(() => {
          const dur = Math.floor(video.duration)
          if (dur > 1 && interval) {
            console.log('have duration');
            clearInterval(interval)
            resolve()
          }
          console.log('await duration');
        }, 1000)
      })
    }
    const video = document.querySelector('.player video');
    const isVideoPaused = document.querySelector('.player.vjs-paused')

    if (!!isVideoPaused) {
      video.play()
    }
    // await sleep(2000)
    const vid2 = document.querySelector('.player video')
    await getDuration(vid2)

    return Math.floor(vid2.duration)
  })
  return duration
}

const visibleActionBar = async (page) => {
  const visible = await page.evaluate(async () => {
    document.querySelector('.control-bar.flex-column').style.display = 'block'
    return true
  })
  return visible
}

const changeOptionVideo = async (page) => {
  const visible = await page.evaluate(async () => {
    const sleep = (ms) => {
      return new Promise((resolve) => setTimeout(resolve, ms));
    }
    document.querySelector('.control-bar.flex-column').classList.add('d-block')
    document.querySelector('.content-player-area button.more-btn .v-btn__content .v-icon').click()
    await sleep(300)
    document.querySelector('.v-menu.v-menu--attached .v-list > div:last-child').click()
    return true
  })
  return visible
}

module.exports = {
  playVideo,
  visibleActionBar,
  changeOptionVideo
}
