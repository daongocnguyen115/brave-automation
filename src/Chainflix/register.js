const {sleep} = require("../actions/utils");
const registerNewAccount = async (page, account) => {
  const accountArr = account.split('|')
  await page.goto('https://www.chainflix.net/member/signup/signupEmail')
  await page.waitForSelector('#__layout');
  await sleep(1000)
  const currentUrl = page.url()

  if (currentUrl === 'https://www.chainflix.net/member/signup/signupEmail') {
    console.log('start signup');

    await page.waitForSelector('#input-19');
    await page.type('#input-19', accountArr[0]);
    await page.type('#input-20', accountArr[1]);
    await sleep(300);
    await page.click('button[type=submit].v-size--large');
    await page.waitForNavigation()
    await page.waitForSelector('.v-content__wrap');
    await sleep(1000);
    await page.waitForSelector('div[class*=birth-day-field]');
    // chọn tháng
    await page.click('div[class*=birth-day-field] > div:first-child > div > .v-input__control > div')
    await sleep(200);
    await page.click('.menuable__content__active .v-list-item:nth-child(1)')
    await sleep(200);
    // chọn ngày
    await page.click('div[class*=birth-day-field] > div:nth-child(2) > div > .v-input__control > div')
    await sleep(200);
    await page.click('.menuable__content__active .v-list-item:nth-child(1)')
    await sleep(200);
    // chọn năm
    await page.click('div[class*=birth-day-field] > div:nth-child(3) > div > .v-input__control > div')
    await sleep(200);
    await page.click('.menuable__content__active .v-list-item:nth-child(7)')
    await sleep(200);
    await page.click('button[type=submit].v-size--large')

    console.log('==============================');
    console.log('signup success');
    console.log('==============================');
    return true
  }
}

module.exports = {
  registerNewAccount
}
