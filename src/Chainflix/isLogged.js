const {sleep} = require("../actions/utils");
const isLogged = async (page, email) => {

  let logged = await page.evaluate(async () => {
    let check = false
    const token = localStorage.getItem('accessToken')
    const refreshToken = localStorage.getItem('refreshToken')
    if (token && refreshToken) {
      check = true
    }

    return check
  })

  if (logged) {
    await page.waitForSelector('.v-form .container > .row > .col:nth-child(2) input')
    const currentEmail = await page.$eval('.v-form .container > .row > .col:nth-child(2) input', element => element.value)
    if (currentEmail !== email) {
      console.log('logout old user: ', currentEmail);
      await logout(page)
      logged = false
    }
  }
  return logged

}

const login = async (page, configs) => {
  const currentUrl = page.url()
  if (!currentUrl.includes('https://www.chainflix.net/member/signin')) {
    await page.goto('https://www.chainflix.net/member/signin')
    await page.waitForNavigation()
  }
  await page.waitForSelector('#input-19');
  await page.type('#input-19', configs.chainFlix.account.email);
  await page.type('#input-20', configs.chainFlix.account.password);
  await sleep(1000);
  await page.click('button[type=submit].purple');
  await page.waitForSelector('.v-content__wrap');
  await sleep(1000);
  console.log('Login success');
}

const logout = async (page) => {
  const isLogout = await page.evaluate(async () => {
    localStorage.setItem('accessToken', '')
    localStorage.setItem('refreshToken', '')
    return true
  })

  return isLogout
}

module.exports = {
  isLogged,
  logout,
  login
}
