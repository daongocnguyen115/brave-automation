const {sleep} = require("../actions/utils");

const {MyBrowser} = require("../actions/utils");
const {changeOptionVideo} = require("./detectPlayVideo");
const {visibleActionBar} = require("./detectPlayVideo");
const {playVideo} = require("./detectPlayVideo");
const {randomLinksChainflix} = require("../actions/utils");
const {detectLogin} = require("./detectLogin");

let configs = {}
let playListUrl = []
let videoIndex = 0

async function chainflixTopic(config) {
  videoIndex = 0
  configs = config
  const user = config.name
  return await main(user, config.chainFlix.soLuongBrowserProfile[0]);
}

async function main(user, profileName = 'Default') {
  const browser = await MyBrowser(user, profileName)
  let pages = await browser.pages()
  var page = pages[0];

  for (let i = 1; i < pages.length; i++) {
    pages[i].close();
  }
  // page.setUserAgent('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36');

  await sleep(1000);

  try {
    await page.goto('https://www.chainflix.net/member/signin');
  } catch (e) {
    console.log('errorrrrr', e);
  }

  await detectLogin(page, configs)

  playListUrl = await randomLinksChainflix('./links.txt')
  playList(page, playListUrl[videoIndex], user)
  return browser
}

async function playList(page, url, userName) {


  try {
    console.log('=============================');
    console.log(`PLAY VIDEO: ${url}`);
    console.log('=============================');
    await page.goto(url)
    // await page.waitForNavigation();
    try {
      await page.waitForSelector('.player video', {timeout: 120000});
    } catch (e) {

    }


    await sleep(2000)

    const video = page.$('.player video')
    if (!video) {
      await sleep(5000)
    }

    const videoDuration = await playVideo(page)
    await sleep(1000)
    await changeOptionVideo(page)

    let sleepTime = videoDuration * 1000 - 60000

    if (sleepTime < (3*60000) || isNaN(sleepTime)) {
      sleepTime = 600000
    }
    console.log(`${userName} - ${url}: sleep`, sleepTime);
    console.log('VIDEO PLAYING....');
    console.log('=============================');
    await sleep(sleepTime)
    if (videoIndex >= playListUrl.length - 1) {
      videoIndex = 0
    } else {
      videoIndex++
    }
    playList(page, playListUrl[videoIndex], userName)
  } catch (e) {
    console.log('da co loi', e);
    playList(page, playListUrl[videoIndex], userName)
  }
}

module.exports = {
  chainflixTopic
}
