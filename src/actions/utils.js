const puppeteer = require("puppeteer-extra");
// Add stealth plugin and use defaults (all tricks to hide puppeteer usage)
const StealthPlugin = require("puppeteer-extra-plugin-stealth");
puppeteer.use(StealthPlugin());


const fs = require("fs");
const sleep = (ms) => {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

const MyBrowser = async (user, profileName) => {
  const browser = await puppeteer.launch({
    headless: true,
    ignoreHTTPSErrors: true,
    args: [
      `--window-size=1024,700`,
      "--no-sandbox",
      "--disable-setuid-sandbox",
      "--disable-web-security",
      "--disable-automation",

      '--test-type',
      `--profile-directory=${profileName}`
    ],
    ignoreDefaultArgs: ["--disable-extensions","--enable-automation"],
    defaultViewport: null,
    executablePath: `${process.env.BROWSER_APPLICATION_PATH}`,
    userDataDir: `${process.env.BROWSER_PROFILE_PATH}/${user}`,
  });
  return browser
}

const randomScrollArr = (height) => {
  const bodyHeight = height
  const splitNumber = 20
  const space = bodyHeight/splitNumber

  const arr = [];
  for (let i = 0; i < splitNumber; i++) {
    arr.push(i*space)
  }

  let randArr = []
  for (let i = 0; i < 5; i++) {
    randArr.push(arr[Math.floor(Math.random() * arr.length)])
  }
  return randArr
}

const randomLinksChainflix = (filePath) => {
  try {
    const data = fs.readFileSync(filePath, 'utf8')
    const array = data.match(/[^\r\n]+/g);
    return shuffle(array)
  } catch (err) {
    console.error(err)
    return []
  }
}

const readFileData = (filePath) => {
  try {
    const data = fs.readFileSync(filePath, 'utf8')
    const array = data.match(/[^\r\n]+/g);
    return array
  } catch (err) {
    console.error(err)
    return []
  }
}

const getFileData = async (url) => {
  const http = require("http");
  // const file = fs.createWriteStream("file.docx");

  await http.get(url, response => {
    // response.pipe(file);
    console.log('', response);
    console.log('get file done');
  });
  console.log('function get file done');
}

// random array index
const shuffle = (array) => {
  let currentIndex = array.length,  randomIndex;

  // While there remain elements to shuffle...
  while (currentIndex != 0) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex--;

    // And swap it with the current element.
    [array[currentIndex], array[randomIndex]] = [
      array[randomIndex], array[currentIndex]];
  }

  return array;
}

module.exports = {
  sleep,
  randomScrollArr,
  readFileData,
  randomLinksChainflix,
  shuffle,
  MyBrowser,
  getFileData
}
