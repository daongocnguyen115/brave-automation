const {autoScroll} = require("./scroll");
const getRandomUrl = (arrDom) => {
  console.log('arrDom', arrDom);
  const titleList = arrDom
  const urlArr = []
  titleList.map(item => {
    const element = item.getElementsByTagName("a")[0];
    const url = item.getElementsByTagName("a")[0]?.getAttribute('href');
    if (url && url !== 'javascript:void(0);' && url !== '/') {
      urlArr.push(element)
    }
  })
  return urlArr[Math.floor(Math.random() * urlArr.length)]
}

const autoNavigate = async (page) => {
  // await page.exposeFunction("getRandomUrl", getRandomUrl);

  await page.evaluate(async () => {
    const arrDom = await Array.from(document.querySelectorAll('h2, h3, h4'))
    if (!arrDom || arrDom.length <= 0) return
    const titleList = arrDom
    const urlArr = []
    await titleList.map(item => {
      const element = item.getElementsByTagName("a")[0];
      const url = item.getElementsByTagName("a")[0]?.getAttribute('href');
      if (url && url !== 'javascript:void(0);' && url !== '/') {
        urlArr.push(element)
      }
    })

    const url = urlArr[Math.floor(Math.random() * urlArr.length)]
    // const url = await getRandomUrl(arrDom)
    console.log('=================================');
    console.log('click url: ', url);
    console.log('=================================');
    try {
      await url.click();
    } catch (e) {
      console.log('url click', e);
    }
  })

  await page.waitFor(2000);
  await autoScroll(page)
}

module.exports = {
  autoNavigate,
  getRandomUrl
}
