const {randomScrollArr} = require("./utils");
const autoScroll = async (page) => {
  await page.exposeFunction("randomScrollArr", randomScrollArr);
  await page.evaluate(async () => {
    const rand = await randomScrollArr(document.body.scrollHeight)
    if (!rand || rand.lenght <= 0) return false
    await new Promise((resolve, reject) => {
      let index = 0
      let timer = setInterval(() => {
        let position = rand[index]
        window.scrollTo({
          top: position,
          left: 0,
          behavior: 'smooth',
        });
        index++
        if(index >= rand.length){
          clearInterval(timer);
          resolve();
        }
      }, 1500);
    });
  });
}

module.exports = {
  autoScroll
}
