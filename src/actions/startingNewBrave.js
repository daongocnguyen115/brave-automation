const onNewBrave = async (page) => {
  await page.evaluate(async () => {
    const sleep = (ms) => {
      return new Promise((resolve) => setTimeout(resolve, ms));
    }
    const btnActiveAds = document.querySelectorAll("button[type=accent]");
    const btnActiveNews = document.querySelectorAll("section.Section-sc-1auuhto button[class^=Button-sc-]");

    await sleep(1000)
    // cho phép load tin tức
    if (btnActiveNews.length > 0) {
      btnActiveNews[0].click()
    }
    await sleep(1000)

    // mở brave reward
    if (btnActiveAds.length > 0) {
      btnActiveAds[0].click()
    }
  })
}

module.exports = {
  onNewBrave
}
