const campaignDetect = async (page) => {
  let campaign = ''
  const campaignName = function(val) {
    campaign = val
  };
  await page.exposeFunction("campaignName", campaignName);

  await page.evaluate(async () => {

    const backgroundAds = document.querySelector("section[class^=GridItemCredits-sc-]");
    await campaignName('backgroundAds')
  })

  console.log('campaign', campaign);
  return campaign
}

module.exports = {
  campaignDetect
}
