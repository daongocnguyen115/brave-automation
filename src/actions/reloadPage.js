const {sleep} = require("./utils");

const reloadPage = async (page, reloadNumber) => {

  let count = 1
  let interval = setInterval(() => {
    if (count === reloadNumber) clearInterval(interval)
    page.reload({ waitUntil: ["networkidle0", "domcontentloaded"] });
    count++
  }, 1000)

  await sleep((1000 * reloadNumber) + 1000)
  console.log('============================');
  console.log('task reload done');
  console.log('============================');
  // await page.evaluate(async () => {
  // })
}

module.exports = {
  reloadPage
}
