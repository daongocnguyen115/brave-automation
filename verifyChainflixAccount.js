const puppeteer = require("puppeteer-extra");
// Add stealth plugin and use defaults (all tricks to hide puppeteer usage)
const StealthPlugin = require("puppeteer-extra-plugin-stealth");
const {sleep} = require("./src/actions/utils");
puppeteer.use(StealthPlugin());
require('dotenv').config();

let currentAccountIndex = 0

const account = [
  'ardeen1caoc45@gmail.com|Chanflix999!@#',
];

(async function main() {
  launchBrowser()
})()


async function launchBrowser() {
  const browser = await puppeteer.launch({
    headless: false,
    ignoreHTTPSErrors: true,
    args: [
      // `--window-size=1024,600`,
      "--no-sandbox",
      "--disable-setuid-sandbox",
      "--disable-web-security",
      "--disable-automation",

      // '--test-type',
      `--profile-directory=Default`
    ],
    ignoreDefaultArgs: ["--disable-extensions","--enable-automation"],
    defaultViewport: null,
    executablePath: `${process.env.BROWSER_APPLICATION_PATH}`,
    userDataDir: `${process.env.BROWSER_PROFILE_PATH}/user1`,
  });
  let pages = await browser.pages()

  var page = pages[0];

  for (let i = 1; i < pages.length; i++) {
    pages[i].close();
  }
  await sleep(1000);
  // page.setUserAgent('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36');

  try {
    await page.goto('https://accounts.google.com/ServiceLogin');
    // await page.goto('https://www.google.com/');
  } catch (e) {
    console.log('errorrrrr', e);
  }
  preLogin(browser, page, account[currentAccountIndex])
}

async function preLogin(browser, page, currentAccount) {
  try {
    console.log('==============================');
    console.log('login account: ', currentAccount);
    console.log('==============================');

    const logged = await isLoggedGoogle(page)
    console.log('is logged', logged);
    if (logged) {
      await logoutGoogle(page)
    } else {
      await loginGoogle(page, currentAccount)
    }
    // await registerNewAccount(page, currentAccount)
    // await page.waitForNavigation()
    // await sleep(2000)
    // if (currentAccountIndex < account.length - 1) {
    //   currentAccountIndex++
    //   preLogin(browser, page, account[currentAccountIndex])
    // } else {
    //   await browser.close()
    // }
  } catch (e) {
    console.log('==============================');
    console.log('co loi khi dang ky');
    console.log('==============================');
    console.log(e);
  }
}



/**********************
 * cac ham lien quan
 * *******************/
async function isLoggedGoogle(page) {
  await page.waitForSelector('#view_container');
  let isLogged = true
  // const btnLogin = page.$eval(`a[href*="https://accounts.google.com/AccountChooser/signinchooser?service=mail"]`, element=> element.getAttribute("href"))
  const inputEmail = await page.$('input[type=email]')
  if (inputEmail) {
    isLogged = false
  }
  return isLogged
}

async function logoutGoogle(page) {
  const logoutUrl = page.$eval(`a[href*="https://accounts.google.com/Logout"]`, element=> element.getAttribute("href"))
  console.log('logoutUrl: ',logoutUrl);
}

async function loginGoogle(page, currentAccount) {
  const accountArr = currentAccount.split('|')
  await page.type('input[type="email"]', accountArr[0]); // Email login
  await page.click("#identifierNext");

  await page.waitForSelector('input[type="password"]', { visible: true });
  await page.type('input[type="password"]', accountArr[1]); // Password login

  await page.waitForSelector("#passwordNext", { visible: true });
  await page.click("#passwordNext");
  await page.waitForNavigation();
  await page.goto('https://mail.google.com/mail/?tab=km')
}

async function findVerifyEmail(page) {
  try {
    await page.type('input[aria-label="Search mail"]', '[CHAINFLIX] The verification email you requested')
    await page.click('button[aria-label="Search mail')
    await page.waitForNavigation();

  } catch (e) {

  }
}
