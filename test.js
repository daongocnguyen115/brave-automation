const puppeteer = require('puppeteer');
const fs = require("fs");

// const user = process.argv[2];
// const profile = process.argv[3].replace('_', ' ');
// const url = process.argv[4];

// console.log(user, profile, url);
(async function main() {
  try {
    const data = fs.readFileSync('./links.txt', 'utf8')
    // console.log(data)
    var array = data.match(/[^\r\n]+/g);
    console.log(array);
  } catch (err) {
    console.error(err)
  }
  // try {
  //   botRun(user, profile, url)
  // } catch (e) {
  //   console.log(e)
  // }
})()

async function botRun(user, profile, url) {
  let browser;
  try {
    browser = await puppeteer.launch({
      headless:false,
      ignoreHTTPSErrors: true,
      args: [`--window-size=1820,1000`, '--test-type', `--profile-directory=${profile || 'Default'}`],
      ignoreDefaultArgs: ['--enable-automation'],
      defaultViewport: {
        width:1620,
        height:800
      },
      executablePath:"/Applications/Brave Browser.app/Contents/MacOS/Brave Browser",
      userDataDir: `/Users/ngocnguyen/Documents/Workplace/brave-automation/Profiles/${user || 'user1'}`,
    });
    const page = await browser.newPage();
    await page.goto(url,{timeout: 5000});
    await page.waitForTimeout(500);
    console.log('function ' + url);
  } catch(e) {
    console.log(e);
    // decide what you're doing upon errors here
  } finally {
    if (browser) {
      // await browser.close();
    }
  }
}

// laptop
// ardeen1caoc45@gmail.com	g94BRrYS16G	Chanflix999!@#
//   gretebve3e06@gmail.com	dicdwgo2g2p	Chanflix999!@#
//   nertix1byj37@gmail.com	ueAz7rubKfj	Chanflix999!@#

//may1
//   aarent3skg39@gmail.com	3aaKNCdJgiK	Chanflix999!@#
//   nicol83tup97@gmail.com	UvN25ixvFJd	Chanflix999!@#
//   dolley86htj27@gmail.com	98NyGwhqaxo	Chanflix999!@#
//   rannap1iwa00@gmail.com	4xsiMwjQtkI	Chanflix999!@#
//   ferdinandenr5op50@gmail.com	fjMtCP69OmE	Chanflix999!@#
//   merraleelwb9b11@gmail.com	znr88KvTo3E	Chanflix999!@#
//   donia45vsk00@gmail.com	lh8hvFu5NYT	Chanflix999!@#

// may 2
//   sofiamt6ne15@gmail.com	Ja9TpRxV6fW	Chanflix999!@#
//   constancia8ewry55@gmail.com	jFCwePFLiDO	Chanflix999!@#
//   gabrielliaq4vow38@gmail.com	LKTWA6YMtYT	Chanflix999!@#
//   prissiehkz3i15@gmail.com	sqYjCuc2udA	Chanflix999!@#
//   carmellapoxnh29@gmail.com	7tyJdxcEgta	Chanflix999!@#

// may3
//   phoebejybsh05@gmail.com	hecfA89Gt7N	Chanflix999!@#
//   vonny4mkvo00@gmail.com	N8vSL54agMV	Chanflix999!@#
//   carlitajhnds24@gmail.com	GHKZqCHO7pC
// petralhrju79@gmail.com	WpNEKaIGozT
// michelejslty11@gmail.com	bySxlulV4JE
