require('dotenv').config();
const puppeteer = require("puppeteer-extra");

// Add stealth plugin and use defaults (all tricks to hide puppeteer usage)
const StealthPlugin = require("puppeteer-extra-plugin-stealth");
puppeteer.use(StealthPlugin());

// Add adblocker plugin to block all ads and trackers (saves bandwidth)
const AdblockerPlugin = require("puppeteer-extra-plugin-adblocker");
puppeteer.use(AdblockerPlugin({ blockTrackers: true }));

function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}
(async function () {
  // That's it, the rest is puppeteer usage as normal
  const browser = await puppeteer.launch({
    headless: false,
    //args:['--user-data-dir=F:\\Freelance\\AnhDam\\11092201\\Gmail_Tool\\profiles']
    userDataDir: "F:/Freelance/AnhDam/11092201/Gmail_Tool/profiles/user1"
  });
  const page = await browser.newPage();
  let navigationPromise = page.waitForNavigation();

  await page.goto("https://accounts.google.com/");
  await navigationPromise;
  await page.waitForSelector('input[type="email"]');
  await page.type('input[type="email"]', 'thanhhang28032003@gmail.com'); // Email login
  await page.click("#identifierNext");

  await page.waitForSelector('input[type="password"]', { visible: true });
  await page.type('input[type="password"]', 'Hang28032003'); // Password login

  await page.waitForSelector("#passwordNext", { visible: true });
  await page.click("#passwordNext");
  navigationPromise = page.waitForNavigation();
  await navigationPromise;
  //await page.goto(process.env.file_url); // Spreadsheet url

  //await page.screenshot({ path: "spreadsheet-screen.png", fullPage: true }); // We take a screenshot to have probe of the bypass
  await browser.close();
})();
