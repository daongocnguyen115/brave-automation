const {getFileData} = require("./src/actions/utils");
const {getConfig} = require("./configs");
const {chainflixTopic} = require("./src/Chainflix");
// const {CONFIG} = require("./configs");
const { sleep } = require("./src/actions/utils");
require('dotenv').config();


(async function main() {
  console.log('starting.....');
  console.log('================================');
  // await getFileData('http://pharma.daongocnguyen.com/links.txt')
  const configs = await getConfig()
  for (let i = 0; i <= configs.length -1; i++) {
    console.log('Start user', configs[i].chainFlix.account.email);
    console.log('================================');
    await chainflixTopic(configs[i])
    await sleep( (i + 1)*60000)
  }
})()
