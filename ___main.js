const {chainflixTopic} = require("./src/Chainflix");
const {CONFIG} = require("./configs");
const {braveTopic} = require("./src/BraveTopic");
const { sleep } = require("./src/actions/utils");
require('dotenv').config();


(async function main() {
  CONFIG.browser.map( async (config, index) => {
    await sleep( index*80000)
    const braveCampaign = config.brave
    const chainFlix = config.chainFlix
    let currentBrowserProfile = 0
    let browser = undefined
    if (braveCampaign) {
      await braveTopic(config)
    }
    if (chainFlix) {
      browser = await chainflixTopic(config)
    }
    if (config.chainFlix?.thoiGianDoiProfile) {
      setInterval(async () => {
        if (browser) {
          await browser.close();
        }
        if (braveCampaign) {
          await braveTopic(config)
        }
        if (chainFlix) {
          if (currentBrowserProfile < config.chainFlix.soLuongBrowserProfile.length - 1) {
            currentBrowserProfile++
          } else {
            currentBrowserProfile = 0
          }
          browser = await chainflixTopic(config, config.chainFlix.soLuongBrowserProfile ? config.chainFlix.soLuongBrowserProfile[currentBrowserProfile] : 'Default')
        }
      }, config.chainFlix.thoiGianDoiProfile)
    }

  })
  console.log('================================');
  console.log('All starting.....');
  console.log('================================');
})()
