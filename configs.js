const {readFileData} = require("./src/actions/utils");

async function getConfig() {
  const accounts = await readFileData('./accountLogin.txt')
  const res = accounts.map((item, index) => {
    const [userName, password] = item.split('|')
    return {
      name: `user${index + 1}`,
      chainFlix: {
        soLuongBrowserProfile: ['Default'],
        maxTimePlayVideo: 1800000,
        account: {email: userName, password: password}
      },
    }
  })
  return res
}

module.exports = {
  getConfig
}
