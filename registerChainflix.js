const puppeteer = require("puppeteer-extra");
// Add stealth plugin and use defaults (all tricks to hide puppeteer usage)
const StealthPlugin = require("puppeteer-extra-plugin-stealth");
const {readFileData} = require("./src/actions/utils");
const {MyBrowser} = require("./src/actions/utils");
const {sleep} = require("./src/actions/utils");
const {registerNewAccount} = require("./src/Chainflix/register");
const {logout} = require("./src/Chainflix/isLogged");
const {isLogged} = require("./src/Chainflix/isLogged");
puppeteer.use(StealthPlugin());
require('dotenv').config();

let currentAccountIndex = 0

let account = [];

(async function main() {
  account = await readFileData('./accountRegister.txt')
  if (account.length <= 0) {
    return console.log('khong co tai khoan nao');
  }
  launchBrowser()
})()


async function launchBrowser() {
  const browser = await MyBrowser('user1', 'Default')
  let pages = await browser.pages()
  var page = pages[0];

  for (let i = 1; i < pages.length; i++) {
    pages[i].close();
  }
  await sleep(1000);

  try {
    await page.goto('https://www.chainflix.net/member/signin');
  } catch (e) {
    console.log('errorrrrr', e);
  }
  preRegister(browser, page, account[currentAccountIndex])
}

async function preRegister(browser, page, currentAccount) {
  try {
    console.log('==============================');
    console.log('signup account: ', currentAccount);
    console.log('==============================');

    await page.waitForSelector('#__layout');
    const logged = await isLogged(page, currentAccount)
    console.log('is logged', logged);
    if (logged) {
      await logout(page)
    }
    await registerNewAccount(page, currentAccount)
    await sleep(5000)
    if (currentAccountIndex < account.length - 1) {
      currentAccountIndex++
      preRegister(browser, page, account[currentAccountIndex])
    } else {
      await browser.close()
    }
  } catch (e) {
    console.log('==============================');
    console.log('co loi khi dang ky: ', account[currentAccountIndex]);
    console.log('==============================');
    console.log(e);
  }
}
